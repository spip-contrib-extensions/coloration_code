# Changelog

## 1.0.0 - 2023-06-09

### Changed

- Nécessite SPIP 4.2 minimum
- ! Utilise hljs (v11.7) en javascript (à la place de Geshi en PHP)
- ! Aucune constante de v0.x n’est prise en compte
<?php
return [
	// C
	'configurer' => 'Configurer Coloration Code',
	// G
	'grammaires_chargement' => 'Méthode de chargement',
	'grammaires_chargement_explication' => 'Par défaut, Coloration Code fonctionne en mode "à la demande" : chaque langage détecté dans une page est téléchargé dynamiquement. Mais de la sorte, vous ne bénéficiez pas des mécanismes de concaténation, de minifcation et de cache des scripts proposé par SPIP.
	Il est donc possible de basculer en mode "statique" et de sélectionner les syntaxes présentes sur votre site ; elles seront chargées sur l\'ensemble de votre site mais profiteront des mécanismes évoqués ci-dessus.',
	'grammaires_chargement_dynamique' => 'Dynamique',
	'grammaires_chargement_statique' => 'Statique',
	'grammaires_choix' => 'Sélection des langages',
	'grammaires_fieldset' => 'Langages',
	// T
	'themes_choix' => 'Sélection du thème',
	'themes_extra' => 'Styles additionnels',
	'themes_extra_explication' => 'Élimine le text-shadow des extraits de code',
	'themes_extra_false' => 'Ne pas activer les styles additionnels',
	'themes_extra_true' => 'Activer les styles additionnels',
	'themes_fieldset' => 'Thème',
];
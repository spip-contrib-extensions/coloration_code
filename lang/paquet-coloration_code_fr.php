<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/coloration_code.git

return [

	// C
	'coloration_code' => 'Coloration Code',
	'coloration_code_description' => 'Coloration Code utilise la librairie [HighlightJS->https://highlightjs.org/] pour effectuer la coloration syntaxique de près de 200 langages différents.
	Par défaut, le plugin fonctionne en mode "dynamique", à la demande : chaque nouvelle syntaxe détectée au sein d’une page déclenche le téléchargement du fichier JavaScript correspondant. 
	Cependant, il est possible de privilégier le mode "statique" : il suffit de définir la liste des grammaires utilisées au sein de votre site ; les scripts correspondants seront alors chargés sur toutes vos pages mais ils profiteront de la gestion du cache de SPIP.	
	La page de configuration du plugin vous permet également de choisir le thème à appliquer à vos extraits de code.',
	'coloration_code_slogan' => 'Du code lisible !',
];

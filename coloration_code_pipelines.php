<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Inserer des styles dans la partie publique
 *
 * @param string $flux
 *
 * @return string
 */
function coloration_code_insert_head_css(string $flux): string {
	$flux .= coloration_code_css();

	return $flux;
}

/**
 * Inserer des scripts dans la partie publique
 *
 * @param string $flux
 *
 * @return string
 */
function coloration_code_insert_head(string $flux): string {
	$flux .= coloration_code_js();

	return $flux;
}


/**
 * Inserer des styles et des scripts dans le privé
 *
 * @param string $flux
 *
 * @return string
 */
function coloration_code_header_prive($flux) {
	$flux .= coloration_code_css();
	$flux .= coloration_code_js();

	return $flux;
}


/**
 * Générer une chaîne contenant les scripts à charger
 *
 * @return string
 */
function coloration_code_js(): string {
	$assets_directory = _DIR_PLUGIN_COLORATION_CODE . 'assets/';
	$js = <<<JAVASCRIPT
	<script type="text/javascript">
		var spipConfig = spipConfig || {};
		spipConfig.coloration_code = {
			assets: '{$assets_directory}'
		}
	</script>
	JAVASCRIPT;

	$js .= "\n" . '<script type="text/javascript" src="' . $assets_directory . 'highlightjs/highlight.min.js"></script>';

	$config = lire_config('coloration_code', []);
	$methode = $config['grammaires_chargement'] ?? 'dynamique';

	if ($methode === 'statique') {
		$syntaxes = $config['grammaires_choix'] ?? [];

		foreach ($syntaxes as $syntaxe) {
			$js .= "\n" . '<script type="text/javascript" src="' . $assets_directory . 'highlightjs/' . $syntaxe . '"></script>';
		}

		$js .= "\n" . '<script type="text/javascript" src="' . $assets_directory . 'coloration_code__static.js"></script>';
	} elseif ($methode === 'dynamique') {
		$js .= "\n" . '<script type="text/javascript" src="' . $assets_directory . 'coloration_code__on_demand.js"></script>';
	}

	return $js;
}


/**
 * Générer une chaîne contenant les feuilles de styles à charger
 *
 * @return string
 */
function coloration_code_css(): string {
	$css = '';

	$config = lire_config('coloration_code', []);
	$theme = $config['themes_choix'] ?? 'styles/a11y-dark.min.css';
	$path = 'assets/highlightjs/' . $theme;

	$css .= "\n" . '<link rel="stylesheet" href="' . direction_css(_DIR_PLUGIN_COLORATION_CODE . $path) . '" type="text/css" media="all" />';


	$extras = $config['themes_extra'] ?? true;

	if ($extras) {
		$css .= "\n" . '<link rel="stylesheet" href="' . direction_css(_DIR_PLUGIN_COLORATION_CODE . 'assets/coloration_code_extra.css') . '" type="text/css" media="all" />';
	}

	return $css;
}

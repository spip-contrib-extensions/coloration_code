<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Gestion des saisies pour génération du formulaire de configuration
 *
 * @return array<mixed> $saisies
 */
function formulaires_configurer_coloration_code_saisies_dist() {
	// $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration
	$saisies = [
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'grammaires_fieldset',
				'label' => '<:coloration_code:grammaires_fieldset:>',
			],
			'saisies' => [
				[
					'saisie' => 'radio',
					'options' => [
						'nom' => 'grammaires_chargement',
						'label' => '<:coloration_code:grammaires_chargement:>',
						'explication' => '<:coloration_code:grammaires_chargement_explication:>',
						'defaut' => 'dynamique',
						'datas' => [
							'statique' => '<:coloration_code:grammaires_chargement_statique:>',
							'dynamique' => '<:coloration_code:grammaires_chargement_dynamique:>',
						]
					]
				],
				[
					'saisie' => 'checkbox',
					'options' => [
						'afficher_si' => '@grammaires_chargement@=="statique"',
						'nom' => 'grammaires_choix',
						'label' => '<:coloration_code:grammaires_choix:>',
						'explication' => '<:coloration_code:grammaires_choix_explication:>',
						'datas' => lister_assets('languages')
					]
				],
			],
		],
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'themes_fieldset',
				'label' => '<:coloration_code:themes_fieldset:>',
			],
			'saisies' => [
				[
					'saisie' => 'selection',
					'options' => [
						'nom' => 'themes_choix',
						'label' => '<:coloration_code:themes_choix:>',
						'defaut' => 'styles/a11y-dark.min.css',
						'datas' => lister_assets('styles')
					]
				],
				[
					'saisie' => 'radio',
					'options' => [
						'nom' => 'themes_extra',
						'label' => '<:coloration_code:themes_extra:>',
						'explication' => '<:coloration_code:themes_extra_explication:>',
						'datas' => [
							false => '<:coloration_code:themes_extra_false:>',
							true => '<:coloration_code:themes_extra_true:>'
						],
						'defaut' => true,
					]
				]
			],
		],
	];
	return $saisies;
}

function lister_assets(string $type): array {
	$path = _DIR_PLUGIN_COLORATION_CODE . 'assets/highlightjs/';

	// On récupère l'ensemble des fichiers selon le type de ressource
	$raw = find_all_in_path($path . $type . '/', '', true);

	$result = [];

	foreach ($raw as $k => $v) {
		$key = substr($k, 0, strpos($k, '.min.'));
		$value = substr($v, strpos($v, $path) + strlen($path));

		$result[$value] = $key;
	}

	asort($result);

	return $result;
}

(() => {
  const onReady = function (fn) {
    if (document.readyState !== 'loading') {
      fn();
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
  }  
  const startHljs = function () {
    const blocks = document.querySelectorAll('pre code:not(.hljs)');
    blocks.forEach(e => e.parentElement.dataset.highlight = 'hljs');
    blocks.forEach(hljs.highlightElement);
  }

  onReady(startHljs)
  onAjaxLoad(startHljs);
})();

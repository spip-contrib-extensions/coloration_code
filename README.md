# Coloration Code

Le plugin Coloration Code pour SPIP 4.2+ bascule d'un traitement côté serveur avec GeSHi à la librairie JavaScript [highlight.js](https://highlightjs.org/) qui prend en charge près de 200 langages et propose près de 250 thèmes.

## Usage

SPIP 4.2 introduit le raccourci triple backtips (<code>```</code>) pour délimiter des extraits de code. Il suffit de préciser le langage correspondant à votre extrait de code pour que celui-ci soit intégré au markup généré ; ainsi pour du HTML :

```html
<pre class="spip_code spip_code_block language-html" data-language="html">
    <code>
        ...
    </code>
</pre>
```

Coloration Code se sert ainsi par exemple de l'attribut <code>data-language</code> généré par SPIP.

## Configuration

Coloration Code fonctionne "out of the box" grâce à sa méthode de chargement **dynamique**, activée par défaut ; celle-ci permet de charger automatiquement toutes les syntaxes repérées au sein d'une page.

Cette méthode, si elle a l'avantage de permettre de suite de voir si le plugin fonctionne, reste problématique : en effet, les scripts des grammaires ainsi téléchargés ne passent pas au travers des mécanismes de concaténation, minification et surtout de cache de SPIP.

Il peut donc être plus intéressant d'utiliser la méthode **statique** qui nécessite de sélectionner les grammaires qui seront alors chargées sur toutes les pages de votre site mais qui passeront par les outils décrits ci-dessus.

## Développement

L'ensemble des scripts de langage et des thèmes de highlight.js présents dans le répertoire `assets/highlightjs/` de ce plugin sont une copie des fichiers générés par la compilation des sources de la librairie.

Une fois ce dépôt cloné, il faut récupérer la librairie highlight.js, déclarée en *submodule* de GIT :

```shell
$ cd lib/highlight.js
$ git submodule init
$ git submodule update
```

Une fois les sources de highlight.js récupérées, il faut installer les dépendances nécessaires à leur compilation :

```shell
$ npm install
```
Puis on lance la commande 
```shell
$ node tools/build.js -t cdn
```
Les fichiers compilés et minifiés sont alors disponible dans le répertoire `lib/highlight.js/build/`.

### Développer la syntaxe SPIP

L'écriture d'un langage pour highlight.js permettant de colorer la syntaxe des squelettes de SPIP est en cours ; le code en est disponible sur la forge de développement ([https://git.spip.net/spip-contrib-outils/highlightjs-spip](https://git.spip.net/spip-contrib-outils/highlightjs-spip)) et elle est intégrée comme submodule GIT de ce plugin.

Pour récupérer le code source :

```shell
$ cd lib/highlightjs-spip
$ git submodule init
$ git submodule update
```

Il convient alors de faire un lien symbolique de ce répertoire `lib/highlightjs-spip` vers `lib/highlight.js/extra` :

```shell
$ ln -s $PWD/lib/highlightjs-spip $PWD/lib/highlight.js/extra
```

Pour lancer la compilation des langages en mode développement, il faut utiliser la commande suivante depuis le répertoire de la lib, `lib/highlight.js` :

```shell
npm run-script build-browser spip
```

On peut alors utiliser l'outil fourni par highlight.js (`lib/highlight.js/tools/developer.html`) pour développer notre grammaire.


Ensuite, il convient de tester le langage :

```shell
$ npm run-script build_and_test
```